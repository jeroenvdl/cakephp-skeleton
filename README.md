# cakephp-skeleton

A skeleton for creating applications with [CakePHP](https://cakephp.org) 3.x. based on nginx and PHP FPM.

## Setup

First, make sure that you have installed the latest version of [Docker](https://www.docker.com/get-docker) and [Docker Compose](https://docs.docker.com/compose/install/#install-compose).

To download the complete environment, including the CakePHP, clone the repository and checkout the submodules recursively.

```bash
git clone --recurse-submodules git@gitlab.com:jeroenvdl/cakephp-skeleton.git
```

After everything finished downloading, you can start CakePHP using Docker Compose

```bash
docker-compose up
```

This command will automatically build the containers, fetch dependencies using Composer and start a web server at [http://localhost:8080](http://localhost:8080).

From now on, you can work in the app directory, according to the [CakePHP Cookbook](https://book.cakephp.org/3.0/en/index.html).