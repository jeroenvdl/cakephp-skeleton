FROM php:7.4-fpm

RUN apt-get update && apt-get install -y \
    libicu-dev \
    libxml2-dev

RUN docker-php-ext-install intl simplexml mysqli xml pdo_mysql

WORKDIR /app