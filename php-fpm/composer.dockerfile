FROM composer:1.10.7

RUN apk update && apk add --no-cache \
    icu-dev \
    libxml2-dev

RUN docker-php-ext-install intl simplexml mysqli xml pdo_mysql
